package com.cream.cream

import androidx.multidex.MultiDexApplication
import timber.log.Timber

class CreamApplication: MultiDexApplication() {


    override fun onCreate() {
        super.onCreate()

        // Show Timber log message on debug mode
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}